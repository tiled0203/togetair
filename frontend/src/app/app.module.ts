import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NavComponent} from './components/nav/nav.component';
import {RouterModule} from '@angular/router';
import {RouterRoutingModule} from './router/router-routing.module';
import {HttpClientModule} from '@angular/common/http';
import {FaIconLibrary, FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {faInfo, faPlane, faPlus, faTrash} from '@fortawesome/free-solid-svg-icons';
import {CreateBookingComponent} from './components/booking/create-booking/create-booking.component';
import { FlightDetailComponent } from './components/flights/detail-flight/flight-detail.component';
import { ListBookingComponent } from './components/booking/list-booking/list-booking.component';
import { LoginComponent } from './components/admin/login/login.component';
import { RegisterComponent } from './components/admin/register/register.component';
import { AddAirportComponent } from './components/airports/add-airport/add-airport.component';
import {FlightsComponent} from './components/flights/list-flight/flights.component';
import { ListAirportComponent } from './components/airports/list-airport/list-airport.component';
import { AppSettingsComponent } from './components/admin/app-settings/app-settings.component';
import { AddFlightComponent } from './components/flights/add-flight/add-flight.component';
import { ReportingComponent } from './components/admin/reporting/reporting.component';
import { ExportingComponent } from './components/admin/exporting/exporting.component';

@NgModule({
  declarations: [
    AppComponent,
    FlightsComponent,
    NavComponent,
    CreateBookingComponent,
    FlightDetailComponent,
    ListBookingComponent,
    LoginComponent,
    RegisterComponent,
    AddAirportComponent,
    ListAirportComponent,
    AppSettingsComponent,
    AddFlightComponent,
    ReportingComponent,
    ExportingComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    NgbModule,
    RouterRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    FontAwesomeModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(private library: FaIconLibrary) {
    library.addIcons(faPlane, faInfo, faPlus, faTrash);
  }
}
