import {Airport} from './Airport';
import {Airline} from './Airline';
import {Price} from './Price';

export enum TravelingClass {
  BUSINESS = 'Business', FIRST_CLASS = 'First class', ECONOMY = 'Economy'
}

export enum FlightEvent {
  WEEKEND = 'Weekend', HOLIDAY = 'Holiday', NIGHTLY = 'Nightly'
}

export class Flight {
  [x: string]: {};

  id: string;
  airline: Airline;
  flightNumber: string;
  departure: Airport;
  destination: Airport;
  departureTime: Date;
  arrivalTime: Date;
  availableSeatsForClass: { TravelingClass, number }; // {BUSINESS : 10}
  price: {} = {};
  ticketPrice: Map<TravelingClass, number>; // {key:"BUSINESS", value:"10"}
}
