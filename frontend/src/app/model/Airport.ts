export interface Airport {
  countryCode: string;
  country: string;
  airportCode: string;
  name: string;
}
