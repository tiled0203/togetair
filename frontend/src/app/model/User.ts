export enum Role {
    CUSTOMER = 'CUSTOMER', EMPLOYEE = 'EMPLOYEE', AIRLINE = 'AIRLINE',
}

export class User {
    name: string;
    password: string;
    role: Role;
    userId: string;
}
