export interface AppSettings {
  settingKey: string;
  settingValue: string;
}
