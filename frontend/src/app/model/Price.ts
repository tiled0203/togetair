import {TravelingClass} from './Flight';

export class Price {
  basePrises: { travelingClass: TravelingClass, price };
  volumeDiscounts: { volume: number, discount: number };
}
