import {Flight, TravelingClass} from './Flight';
import {User} from './User';

export class Ticket {
  passenger: string;
}

export class Booking {
  flightNumber: string;
  tickets: Ticket[] = []; // [{passenger:"blabla"}]
  travelingClass: TravelingClass;
  paymentStatus: string;
  customer: User = new User();
  flight: Flight;
  price: number;
}
