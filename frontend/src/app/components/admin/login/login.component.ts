import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {UserService} from '../../../services/user.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, OnDestroy {
  loginForm: FormGroup;
  private redirectRoute: string;
  private loginSubscription: Subscription;

  constructor(private formBuilder: FormBuilder, private loginService: UserService, private activatedRoute: ActivatedRoute, private router: Router) {
    this.loginForm = this.formBuilder.group({
      name: this.formBuilder.control('')
      , password: this.formBuilder.control('')
    });
    this.activatedRoute.queryParams.subscribe(value => this.redirectRoute = value.return);
  }

  ngOnInit(): void {

  }

  login(): void {
    this.loginService.login(this.loginForm.value);
    this.loginSubscription = this.loginService.user$.subscribe(value => {
      if (value) {
        this.router.navigateByUrl(this.redirectRoute);
      }
    });
  }

  gotoRegister($event: Event): void {
    $event.preventDefault();
    this.router.navigate(['login/register'], {queryParams: {return: this.redirectRoute}});
  }

  ngOnDestroy(): void {
    if (this.loginSubscription !== undefined) {
      this.loginSubscription.unsubscribe();
    }
  }
}
