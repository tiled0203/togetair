import {Component, OnInit} from '@angular/core';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {UserService} from '../../../services/user.service';
import {AppSettings} from '../../../model/AppSettings';
import {valueReferenceToExpression} from '@angular/compiler-cli/src/ngtsc/annotations/src/util';

@Component({
  selector: 'app-app-settings',
  templateUrl: './app-settings.component.html',
  styleUrls: ['./app-settings.component.css']
})
export class AppSettingsComponent implements OnInit {
  settingsForm: FormGroup;
  settings: FormArray;

  constructor(private userService: UserService, private formBuilder: FormBuilder) {
    this.buildForm();
  }

  private buildForm(): void {
    this.settingsForm = this.formBuilder.group({
      settings: this.formBuilder.array([])
    });
  }

  createItem(settingKey: string, settingValue: string): FormGroup {
    return this.formBuilder.group({
      settingKey: this.formBuilder.control(settingKey),
      settingValue: this.formBuilder.control(settingValue)
    });
  }

  ngOnInit(): void {
    this.getSettings();
  }

  getSettings(): void {
    if (this.settings !== undefined) {
      this.settings.clear();
    }
    this.userService.getSettings().toPromise().then(settings => {
      settings.forEach(value => {
        this.settings = this.settingsForm.get('settings') as FormArray;
        this.settings.push(this.createItem(value.settingKey, value.settingValue));
      });
    });
  }

  updateSettings(): void {
    this.settingsForm.value.settings.forEach(value => {
      if (value.settingValue === '') {
        value.settingValue = null;
      }
    });
    this.userService.updateSettings(this.settingsForm.value.settings).toPromise().then(() => this.getSettings());
  }
}
