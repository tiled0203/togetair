import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {UserService} from '../../../services/user.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit, OnDestroy {
  registerForm: FormGroup;
  private redirectRoute: any;
  private loginSubscription: Subscription;

  constructor(private formBuilder: FormBuilder, private loginService: UserService, private activatedRoute: ActivatedRoute, private router: Router) {
    this.registerForm = this.formBuilder.group({
      name: this.formBuilder.control('')
      , password: this.formBuilder.control('')
    });
    this.activatedRoute.queryParams.subscribe(value => this.redirectRoute = value.return);
  }

  ngOnInit(): void {
  }

  register(): void {
    this.loginService.register(this.registerForm.value).subscribe(value => {
      if (value) {
        console.log(this.redirectRoute);
        value.password = this.registerForm.value.password;
        value.name = this.registerForm.value.name;
        this.loginService.login(value);
        this.loginSubscription = this.loginService.user$.subscribe(() => {
          this.router.navigateByUrl(this.redirectRoute);
        });
      }
    });
  }

  ngOnDestroy(): void {
    if (this.loginSubscription !== undefined) {
      this.loginSubscription.unsubscribe();
    }
  }


}
