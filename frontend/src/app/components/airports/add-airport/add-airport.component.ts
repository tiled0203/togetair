import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {AirportService} from '../../../services/airport.service';
import {Observable} from 'rxjs';
import {Airport} from '../../../model/Airport';
import {Flight} from '../../../model/Flight';

@Component({
  selector: 'app-add-flight',
  templateUrl: './add-airport.component.html',
  styleUrls: ['./add-airport.component.css']
})
export class AddAirportComponent implements OnInit {
  addAirportForm: FormGroup;
  airports: Airport[] = [];

  constructor(private formBuilder: FormBuilder, private airportService: AirportService) {
    this.addAirportForm = formBuilder.group({
      name: formBuilder.control(''),
      airportCode: formBuilder.control(''),
      country: formBuilder.control(''),
      countryCode: formBuilder.control('')
    });
  }

  ngOnInit(): void {
    this.getAirports();
  }

  private getAirports(): void {
    this.airportService.getAirports().toPromise().then(value => this.airports = value);
  }

  addAirport(): void {
    this.airportService.addAirport(this.addAirportForm.value).toPromise().then(() => this.getAirports());
  }
}
