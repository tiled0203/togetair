import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {Airport} from '../../../model/Airport';
import {AirportService} from '../../../services/airport.service';
import {Subscription} from 'rxjs';
import {FlightService} from '../../../services/flight.service';
import {UserService} from '../../../services/user.service';

@Component({
  selector: 'app-list-airport',
  templateUrl: './list-airport.component.html',
  styleUrls: ['./list-airport.component.css']
})
export class ListAirportComponent implements OnInit {
  @Input()
  airports: Airport[] = [];

  constructor(private airportService: AirportService, public loginService: UserService) {
  }

  ngOnInit(): void {
    if (this.airports.length === 0) {
      this.getAirports();
    }
  }

  private getAirports(): void {
    this.airportService.getAirports().toPromise().then(value => this.airports = value);
  }


  delete(airport: Airport): void {
    this.airportService.delete(airport.airportCode).toPromise().then(() => this.getAirports());
  }

}
