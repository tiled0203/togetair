import {Component, Input, OnInit} from '@angular/core';
import {Flight} from '../../../model/Flight';
import {ActivatedRoute} from '@angular/router';
import {FlightService} from '../../../services/flight.service';

@Component({
  selector: 'app-flight-detail',
  templateUrl: './flight-detail.component.html',
  styleUrls: ['./flight-detail.component.css']
})
export class FlightDetailComponent implements OnInit {

  @Input()
  flight: Flight;

  constructor(private activatedRoute: ActivatedRoute, private flightService: FlightService) {
    const flightNumber = activatedRoute.snapshot.paramMap.get('flightNumber');
    if (flightNumber !== undefined) {
      this.flightService.findFlightByFlightNumber(flightNumber).toPromise().then(value => this.flight = value);
    }
  }

  ngOnInit(): void {
  }


}
