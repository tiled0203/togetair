import {Component, OnInit} from '@angular/core';
import {FlightService} from '../../../services/flight.service';
import {Flight, FlightEvent, TravelingClass} from '../../../model/Flight';
import {FormArray, FormBuilder, FormGroup} from '@angular/forms';
import {AirportService} from '../../../services/airport.service';
import {Observable, of} from 'rxjs';
import {catchError, debounceTime, distinctUntilChanged, switchMap, tap} from 'rxjs/operators';
import {Airport} from '../../../model/Airport';
import {Router} from '@angular/router';
import {Price} from '../../../model/Price';

@Component({
  selector: 'app-add-flight',
  templateUrl: './add-flight.component.html',
  styleUrls: ['./add-flight.component.css']
})
export class AddFlightComponent implements OnInit {
  events = Object.keys(FlightEvent).filter(value => isNaN(Number(value)));
  private flightEvents: FormArray;

  flightForm: FormGroup;
  flight: Flight = new Flight();
  searching = false;
  searchFailed = false;
  formatter = (result: Airport) => result.country + ' (' + result.airportCode + ')';
  inputFormat = (airport: Airport) => airport.country;

  constructor(private route: Router, private flightService: FlightService, private formBuilder: FormBuilder, private airportService: AirportService) {
    this.flightForm = formBuilder.group({
      flightNumber: formBuilder.control(''),
      airline: formBuilder.group({name: this.formBuilder.control('')}),
      eventPrice: formBuilder.array([]),
      availableSeatsForClass: formBuilder.group({
        ECONOMY: this.formBuilder.control(''),
        FIRST_CLASS: this.formBuilder.control(''),
        BUSINESS: this.formBuilder.control('')
      }),
      destination: this.formBuilder.control(''),
      departure: this.formBuilder.control(''),
      departureTime: this.formBuilder.control(''),
      arrivalTime: this.formBuilder.control(''),

    });
    this.createItems();

    console.log(this.flightForm);

  }


  airportSearch = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(500),
      distinctUntilChanged(),
      tap(() => this.searching = true),
      switchMap(term => this.airportService.findAirportByAirportCountry(term).pipe(
        tap(() => this.searchFailed = false),
        catchError(() => {
          this.searchFailed = true;
          return of([]);
        }))
      ), tap(() => this.searching = false)
    );

  ngOnInit(): void {
  }

  addFlight(): void {
    let newFlight = new Flight();
    newFlight = this.flightForm.value;
    newFlight.price = {};
    this.flightEvents.value.forEach((item, index) => {
      Object.keys(item).forEach((key) => {
        console.log(this.flightForm.value.eventPrice[index][key]);
        newFlight.price[key] = {basePrises: {}, volumeDiscounts: {}};
        newFlight.price[key].basePrises = this.flightForm.value.eventPrice[index][key];
        newFlight.price[key].volumeDiscounts = this.flightForm.value.eventPrice[index][key].volumeDiscounts;
        newFlight.price[key].basePrises.volumeDiscounts = undefined;
      });
    });

    // console.log(newFlight.price);

    this.flightService.createFLight(newFlight).toPromise().then(() => {
      this.route.navigate(['']);
    });
  }


  private createItems(): any {
    this.flightEvents = this.flightForm.get('eventPrice') as FormArray;

    this.events.forEach((thisVal) => {
      const obj = {};
      obj[thisVal] = this.formBuilder.group({
        ECONOMY: this.formBuilder.control(''),
        FIRST_CLASS: this.formBuilder.control(''),
        BUSINESS: this.formBuilder.control(''),
        volumeDiscounts: this.formBuilder.group({5: this.formBuilder.control(''), 2: this.formBuilder.control('')})
      });
      this.flightEvents.push(this.formBuilder.group(obj));
    });
    console.log(this.flightEvents);
    return this.flightEvents;
  }

  getKeyFromItem(item: any): any {
    return item[0];
  }


}

