import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {Flight} from '../../../model/Flight';
import {FlightService} from '../../../services/flight.service';
import {map} from 'rxjs/operators';
import {UserService} from '../../../services/user.service';

@Component({
  selector: 'app-flights',
  templateUrl: './flights.component.html',
  styleUrls: ['./flights.component.css']
})
export class FlightsComponent implements OnInit {
  flights: Observable<Flight[]> = new Observable<Flight[]>();

  constructor(private flightService: FlightService, public loginService: UserService) {
  }

  ngOnInit(): void {
    this.flights = this.flightService.searchFlights();
  }
}
