import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {BookingService} from '../../../services/booking.service';
import {Booking} from '../../../model/Booking';
import {UserService} from '../../../services/user.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-list-booking',
  templateUrl: './list-booking.component.html',
  styleUrls: ['./list-booking.component.css']
})
export class ListBookingComponent implements OnInit, OnDestroy {
  bookings: Booking[];
  private loginSubscription: Subscription;

  constructor(private bookingService: BookingService, private loginService: UserService) {
  }

  ngOnInit(): void {
    this.loginSubscription = this.loginService.user$.subscribe(user => {
      console.log(user.userId);
      this.bookingService.findBookingByCustomerId(user.userId).toPromise().then(bookingResonse => {
        this.bookings = bookingResonse;
      });
    });
  }

  ngOnDestroy(): void {
    this.loginSubscription.unsubscribe();
  }

}
