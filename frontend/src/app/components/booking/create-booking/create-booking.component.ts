import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {FlightService} from '../../../services/flight.service';
import {Flight, TravelingClass} from '../../../model/Flight';
import {FormArray, FormBuilder, FormGroup, NgForm, Validators} from '@angular/forms';
import {BookingService} from '../../../services/booking.service';
import {Booking, Ticket} from '../../../model/Booking';
import {UserService} from '../../../services/user.service';

@Component({
  selector: 'app-book',
  templateUrl: './create-booking.component.html',
  styleUrls: ['./create-booking.component.css']
})
export class CreateBookingComponent implements OnInit {
  flight: Flight;
  tickets: FormArray;
  bookForm: FormGroup;

  constructor(private loginService: UserService, private router: Router, private activatedRoute: ActivatedRoute, protected flightService: FlightService, private bookingService: BookingService, private formBuilder: FormBuilder) {
    this.bookForm = formBuilder.group({
      travelingClass: this.formBuilder.control('', Validators.required),
      passengers: this.formBuilder.array([this.createItem()])
    });
  }

  createItem(): FormGroup {
    return this.formBuilder.group({
      passenger: this.formBuilder.control('', Validators.required)
    });
  }

  ngOnInit(): void {
    const flightNumber = this.activatedRoute.snapshot.paramMap.get('flightNumber');
    this.flightService.findFlightByFlightNumber(flightNumber).subscribe(flightResult => this.flight = flightResult);
  }

  bookFlight(): void {
    console.log(this.bookForm.controls);
    const booking = new Booking();
    booking.flightNumber = this.flight.flightNumber;
    booking.tickets = this.bookForm.value.passengers;
    booking.travelingClass = this.bookForm.value.travelingClass;
    booking.customer = this.loginService.loggedInUser;
    this.bookingService.bookFlight(booking).subscribe(response => {
      this.router.navigate(['list-booking']);
    });
  }

  addTicket($event: Event): void {
    $event.preventDefault();
    this.tickets = this.bookForm.get('passengers') as FormArray;
    this.tickets.push(this.createItem());
  }


}
