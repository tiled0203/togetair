import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {UserService} from '../../services/user.service';
import {Role, User} from '../../model/User';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {

  constructor(private router: Router, public loginService: UserService) {
  }

  ngOnInit(): void {

  }

  logout(): void {
    this.loginService.logout();
    this.router.navigate(['']);
  }
}
