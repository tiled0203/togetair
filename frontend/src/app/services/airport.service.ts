import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Booking} from '../model/Booking';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';
import {Airport} from '../model/Airport';

@Injectable({
  providedIn: 'root'
})
export class AirportService {

  constructor(private http: HttpClient) {
  }

  addAirport(airport: Airport): Observable<Airport> {
    return this.http.post<Airport>(`${environment.apiUrl}/airport`, airport);
  }

  getAirports(): Observable<Airport[]> {
    return this.http.get<Airport[]>(`${environment.apiUrl}/airport`);
  }

  findAirportByAirportCountry(country: string): Observable<Airport> {
    return this.http.get<Airport>(`${environment.apiUrl}/airport/search`, {params: {country}});
  }

  delete(airportCode: string): Observable<Airport> {
    return this.http.delete<Airport>(`${environment.apiUrl}/airport/${airportCode}`);
  }
}
