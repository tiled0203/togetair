import {Injectable} from '@angular/core';
import {Flight} from '../model/Flight';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Booking} from '../model/Booking';

@Injectable({
  providedIn: 'root'
})
export class BookingService {

  constructor(private http: HttpClient) {
  }

  bookFlight(flight: Booking): Observable<Booking> {
    return this.http.post<Booking>(`${environment.apiUrl}/book`, flight);
  }

  findBookingByCustomerId(userId: string): Observable<Booking[]> {
    return this.http.get<Booking[]>(`${environment.apiUrl}/book`, {params: {customerId: userId}});
  }

  findBookingsByAirlineName(airlineName: string): Observable<Booking[]> {
    return this.http.get<Booking[]>(`${environment.apiUrl}/book/search`, {params: {airlineName}});
  }
}
