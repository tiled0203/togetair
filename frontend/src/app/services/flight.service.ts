import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Flight} from '../model/Flight';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class FlightService {

  constructor(private http: HttpClient) {
  }

  searchFlights(): Observable<Flight[]> {
    return this.http.get<Flight[]>(environment.apiUrl + '/flights/search');
  }

  findFlightByFlightNumber(flightNumber: string): Observable<Flight> {
    return this.http.get<Flight>(`${environment.apiUrl}/flights/${flightNumber}`);
  }


  createFLight(flight: Flight): Observable<Flight> {
    return this.http.post<Flight>(`${environment.apiUrl}/flights`, flight);
  }

}
