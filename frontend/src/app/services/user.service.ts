import {Injectable} from '@angular/core';
import {Role, User} from '../model/User';
import {BehaviorSubject, Observable, Subject} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {AppSettings} from '../model/AppSettings';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  user$: BehaviorSubject<User> = new BehaviorSubject<User>(null);
  loggedInUser: User;

  constructor(private http: HttpClient) {
  }

  login(user: User): void {
    if (this.loggedInUser === undefined && user === null) {
      if (JSON.parse(sessionStorage.getItem('user')) !== null) {
        this.loggedInUser = new User();
        Object.assign(this.loggedInUser, JSON.parse(sessionStorage.getItem('user')));
        this.user$.next(this.loggedInUser);
        this.checkCredentials();
      }
    } else {
      const userPromise = this.http.get<User>(`${environment.apiUrl}/user/${user.name}`).toPromise().then(value => {
        if (value !== null) {
          this.loggedInUser = new User();
          this.loggedInUser.name = value.name;
          this.loggedInUser.password = btoa(user.password);
          this.loggedInUser.userId = value.userId;
          this.loggedInUser.role = value.role;
          this.user$.next(this.loggedInUser);
          sessionStorage.setItem('user', JSON.stringify(this.loggedInUser));
          this.checkCredentials();
        }
      });
    }
  }

  register(user: User): Observable<User> {
    sessionStorage.setItem('user', null);
    return this.http.post<User>(`${environment.apiUrl}/user`, user);
  }

  authenticated(): boolean {
    if (this.loggedInUser !== undefined) {
      return this.checkCredentials();
    } else {
      return false;
    }
  }

  private checkCredentials(): boolean {
    // tslint:disable-next-line:prefer-const
    let pass = btoa('1234');
    if (this.loggedInUser.password === pass) {
      return true;
    } else if (this.loggedInUser.password === pass) {
      return true;
    } else if (this.loggedInUser.password === pass) {
      return true;
    } else {
      return false;
    }
  }

  logout(): void {
    this.loggedInUser = undefined;
    sessionStorage.setItem('user', null);
    this.user$.next(this.loggedInUser);
  }

  updateSettings(appSettings: AppSettings): Observable<AppSettings> {
    return this.http.put<AppSettings>(`${environment.apiUrl}/user/settings`, appSettings);
  }

  getSettings(): Observable<AppSettings[]> {
    return this.http.get<AppSettings[]>(`${environment.apiUrl}/user/settings`);
  }
}
