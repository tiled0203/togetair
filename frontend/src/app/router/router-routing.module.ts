import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {FlightsComponent} from '../components/flights/list-flight/flights.component';
import {CreateBookingComponent} from '../components/booking/create-booking/create-booking.component';
import {FlightDetailComponent} from '../components/flights/detail-flight/flight-detail.component';
import {ListBookingComponent} from '../components/booking/list-booking/list-booking.component';
import {UserService} from '../services/user.service';
import {AuthGuard} from '../services/auth.guard';
import {LoginComponent} from '../components/admin/login/login.component';
import {RegisterComponent} from '../components/admin/register/register.component';
import {AddAirportComponent} from '../components/airports/add-airport/add-airport.component';
import {ListAirportComponent} from '../components/airports/list-airport/list-airport.component';
import {AppSettingsComponent} from '../components/admin/app-settings/app-settings.component';
import {AddFlightComponent} from '../components/flights/add-flight/add-flight.component';

const routes: Routes = [{path: '', component: FlightsComponent}
  , {
    path: 'book-flight/:flightNumber',
    component: CreateBookingComponent,
    canActivate: [AuthGuard],
    data: {allowedRoles: ['CUSTOMER']}
  }
  , {path: 'flight-info/:flightNumber', component: FlightDetailComponent}
  , {path: 'add-flight', component: AddFlightComponent, canActivate: [AuthGuard], data: {allowedRoles: ['AIRLINE']}}
  , {path: 'add-airport', component: AddAirportComponent, canActivate: [AuthGuard], data: {allowedRoles: ['EMPLOYEE']}}
  , {path: 'list-airport', component: ListAirportComponent, canActivate: [AuthGuard], data: {allowedRoles: ['EMPLOYEE', 'AIRLINE']}}
  , {path: 'list-booking', component: ListBookingComponent, canActivate: [AuthGuard], data: {allowedRoles: ['CUSTOMER']}}
  , {path: 'settings', component: AppSettingsComponent, canActivate: [AuthGuard], data: {allowedRoles: ['EMPLOYEE']}}
  , {path: 'login/register', component: RegisterComponent}
  , {path: 'login', component: LoginComponent},
  {path: '**', redirectTo: ''}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class RouterRoutingModule {
}
