package com.vdab.backend.services;

import com.vdab.backend.domain.*;
import com.vdab.backend.repositories.AppSettingsRepository;
import com.vdab.backend.repositories.BookingRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.DayOfWeek;
import java.util.Collections;
import java.util.List;
import java.util.Map;

@Service
@AllArgsConstructor
public class BookingService {
    private final BookingRepository bookingRepository;

    @Transactional
    public Booking createBooking(Booking booking) {
        Flight flight = booking.getFlight();
        Integer totalSeatsForClass = flight.getAvailableSeatsForClass().get(booking.getTravelingClass());
        flight.getAvailableSeatsForClass().put(booking.getTravelingClass(), totalSeatsForClass - booking.getTickets().size());

        booking.setPrice(calculateTicketPrice(flight, booking.getTravelingClass(), booking.getTickets()));
        if (booking.getCustomer().getCustomerCreditCard() == null) {
            booking.setPaymentStatus(PaymentStatus.PENDING);
        } else {
            booking.setPaymentStatus(PaymentStatus.PAID);
        }
        return this.bookingRepository.save(booking);
    }

    public double calculateTicketPrice(Flight flight, TravelingClass travelingClass, List<Ticket> tickets) {
        return calculateTotalPrice(calculatePriceByFlightEvent(flight), travelingClass, tickets);
    }

    public double calculateTicketPrice(Flight flight, TravelingClass travelingClass) {
        return calculateTotalPrice(calculatePriceByFlightEvent(flight), travelingClass, Collections.emptyList());
    }

    private Price calculatePriceByFlightEvent(Flight flight) {
        Price price;
        if (flight.getDepartureTime().getDayOfWeek() == DayOfWeek.SATURDAY || flight.getDepartureTime().getDayOfWeek() == DayOfWeek.SUNDAY) {
            price = flight.getPrice().getOrDefault(Event.WEEKEND, flight.getPrice().get(Event.HOLIDAY));
        } else if (flight.getDepartureTime().getHour() >= 22) {
            price = flight.getPrice().getOrDefault(Event.NIGHTLY, flight.getPrice().get(Event.HOLIDAY));
        } else {
            price = flight.getPrice().getOrDefault(Event.HOLIDAY, flight.getPrice().get(Event.HOLIDAY));
        }
        return price;
    }

    private double calculateTotalPrice(Price priceByEvent, TravelingClass travelingClass, List<Ticket> tickets) {
        int countTickets = !tickets.isEmpty() ? tickets.size() : 1;
        int volumeDiscount = priceByEvent.getVolumeDiscounts().entrySet().stream()
                .filter(entry -> countTickets >= entry.getKey())
                .mapToInt(Map.Entry::getValue).findFirst().orElseGet(() -> 0);
        Double basePrice = priceByEvent.getBasePrises().get(travelingClass);

        double ticketPrice = basePrice - (basePrice * (volumeDiscount / 100.0));
        if (priceByEvent.getPriceMargin() == null) {
            return (countTickets * ticketPrice) + (basePrice + (basePrice * (5 / 100.0)));
        } else {
            return (countTickets * ticketPrice) + priceByEvent.getPriceMargin();
        }
    }

    public List<Booking> findBookingByCustomerId(String customerId) {
        return bookingRepository.findBookingsByCustomer_UserId(customerId);
    }

}
