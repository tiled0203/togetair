package com.vdab.backend.services;

import com.vdab.backend.domain.AppSettings;
import com.vdab.backend.domain.Flight;
import com.vdab.backend.repositories.AppSettingsRepository;
import com.vdab.backend.repositories.FlightRepository;
import com.vdab.backend.domain.dto.FlightCriteria;
import com.vdab.backend.repositories.SearchFlightRepository;
import javassist.NotFoundException;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class FlightService {

    private final FlightRepository flightRepository;
    private final AppSettingsRepository appSettingsRepository;

    public List<Flight> searchFlights(FlightCriteria params) {
        return flightRepository.searchFlights(params);
    }

    public Flight findFlight(Long flightId) throws NotFoundException {
        return flightRepository.findById(flightId).orElseThrow(() -> new NotFoundException("Flight not found"));
    }

    public void save(Flight newFlight) {
        String priceMargin = appSettingsRepository.findAppSettingsBySettingKey(AppSettings.Settings.PRICE_MARGIN).getSettingValue();
        if (priceMargin != null) {
            newFlight.getPrice().values().forEach(price -> price.setPriceMargin(Double.valueOf(priceMargin)));
        }
        flightRepository.save(newFlight);
    }

    public void delete(Flight flight) {
        flightRepository.delete(flight);
    }

    public List<Flight> search2(String airline, String departure, String destination) {
        return flightRepository.searchFlights2(airline, departure, destination);
    }


    public Flight findFlightByFlightNumber(String flightNumber) {
        return flightRepository.findFlightByFlightNumber(flightNumber);
    }
}
