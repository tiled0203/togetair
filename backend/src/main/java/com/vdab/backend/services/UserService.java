package com.vdab.backend.services;

import com.vdab.backend.domain.*;
import com.vdab.backend.repositories.UserRepository;
import javassist.NotFoundException;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

@Service
@AllArgsConstructor
public class UserService {

    private final UserRepository userRepository;

    public WebUser saveUser(WebUser webUser) {
        if (webUser.getUserId() != null && webUser.getRole() == Role.CUSTOMER) {
            userRepository.findById(webUser.getUserId()).ifPresent(userRepository::save);
        } else {
            return userRepository.save(webUser);
        }
        return null;
    }

    public WebUser findCustomer(String username) throws NotFoundException {
        return userRepository.findWebUserByName(username).orElseThrow(() -> new NotFoundException("user not found"));
    }
}
