package com.vdab.backend.services;

import com.vdab.backend.domain.Airport;
import com.vdab.backend.repositories.AirportRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@AllArgsConstructor
public class AirportService {
    private final AirportRepository airportRepository;

    public List<Airport> getAirports() {
        return airportRepository.findAll();
    }

    public Airport createAirport(Airport airport) {
        return airportRepository.save(airport);
    }

    @Transactional
    public void delete(String airportCode) {
        airportRepository.deleteAirportByAirportCode(airportCode);
    }

    public List<Airport> findByCountry(String country) {
        return airportRepository.findAirportsByCountryContaining(country);
    }
}
