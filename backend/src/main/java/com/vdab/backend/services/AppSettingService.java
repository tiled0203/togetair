package com.vdab.backend.services;

import com.vdab.backend.domain.AppSettings;
import com.vdab.backend.repositories.AppSettingsRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class AppSettingService {
    private final AppSettingsRepository appSettingsRepository;

    public void updateSettings(List<AppSettings> appSettings) {
        appSettings.forEach(settings -> {
            AppSettings foundSetting = appSettingsRepository.findAppSettingsBySettingKey(settings.getSettingKey());
            foundSetting.setSettingValue(settings.getSettingValue());
            appSettingsRepository.save(foundSetting);
        });
    }

    public List<AppSettings> getSettings() {
        return appSettingsRepository.findAll();
    }
}
