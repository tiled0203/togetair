package com.vdab.backend.restcontrollers;

import com.vdab.backend.domain.Airport;
import com.vdab.backend.services.AirportService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/airport")
@AllArgsConstructor
public class AirportRestController {

    private final AirportService airportService;

    @GetMapping("")
    public List<Airport> getAirports() {
        return airportService.getAirports();
    }

    @GetMapping("/search")
    public List<Airport> findAirportByCountry(@RequestParam("country") String country) {
        return airportService.findByCountry(country);
    }


    @PostMapping("")
    public ResponseEntity<Airport> addAirport(@RequestBody Airport airport) {
        return new ResponseEntity<>(airportService.createAirport(airport), HttpStatus.CREATED);
    }

    @DeleteMapping("/{airportCode}")
    public void delete(@PathVariable("airportCode") String airportCode) {
        airportService.delete(airportCode);
    }

}
