package com.vdab.backend.restcontrollers;

import com.vdab.backend.domain.Flight;
import com.vdab.backend.domain.Price;
import com.vdab.backend.domain.TravelingClass;
import com.vdab.backend.domain.dto.FlightCriteria;
import com.vdab.backend.domain.dto.FlightDto;
import com.vdab.backend.services.BookingService;
import com.vdab.backend.services.FlightService;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
//Niet @Controller = Spring Mvc
@RequestMapping("/flights")
@AllArgsConstructor
public class FlightRestController {

    private final FlightService flightService;
    private final BookingService bookingService;

    @GetMapping("/{flightNumber}")
    public FlightDto findFlightByFlightNumber(@PathVariable("flightNumber") String flightNumber) {
        Flight flight = flightService.findFlightByFlightNumber(flightNumber);
        FlightDto flightDto = new FlightDto();
        flightDto.setAirline(flight.getAirline());
        flightDto.setArrivalTime(flight.getArrivalTime());
        flightDto.setDepartureTime(flight.getDepartureTime());
        flightDto.setDeparture(flight.getDeparture());
        flightDto.setDestination(flight.getDestination());
        flightDto.setFlightNumber(flight.getFlightNumber());
        Map<TravelingClass, Double> priceMap = new HashMap<>();
        flight.getAvailableSeatsForClass().keySet().forEach(travelingClass -> priceMap.put(travelingClass, bookingService.calculateTicketPrice(flight, travelingClass)));
        flightDto.setTicketPrice(priceMap);
        return flightDto;
    }


    @GetMapping("/search")
    public List<Flight> searchFlights(FlightCriteria params) {
        return flightService.searchFlights(params);
    }

    @PostMapping("")
//    @RolesAllowed("AIRLINE") //TODO: Spring security nog toevoegen en configureren
    public void saveFlight(@RequestBody Flight flight) {
        Flight newFlight = Flight.builder()
                .airline(flight.getAirline())
                .arrivalTime(flight.getArrivalTime())
                .departure(flight.getDeparture())
                .departureTime(flight.getDepartureTime())
                .destination(flight.getDestination())
                .flightNumber(flight.getFlightNumber())
                .availableSeatsForClass(flight.getAvailableSeatsForClass())
                .price(flight.getPrice())
                .build();
        flightService.save(newFlight);
    }

    @GetMapping("/search2")
    //localhost:8080/flights/search2?airline=blabla&departure=blabla&dest=blabla
    public List<Flight> findFlight(@RequestParam(value = "airline", required = false) String airline,
                                   @RequestParam(name = "departure", required = false) String departure,
                                   @RequestParam(name = "destination", required = false) String destination) {
        System.out.println(airline);
        System.out.println(departure);
        System.out.println(destination);
        return flightService.search2(airline, departure, destination);
    }

}
