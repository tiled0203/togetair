package com.vdab.backend.restcontrollers;

import com.vdab.backend.domain.Booking;
import com.vdab.backend.domain.Flight;
import com.vdab.backend.domain.dto.BookingDto;
import com.vdab.backend.services.BookingService;
import com.vdab.backend.services.FlightService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;
import java.util.List;

@RestController
@RequestMapping("/book")
@AllArgsConstructor
public class BookRestController {

    private final BookingService bookingService;
    private final FlightService flightService;

    @PostMapping("")
    public Booking createBooking(@RequestBody BookingDto bookingDto) {
        Flight flight = flightService.findFlightByFlightNumber(bookingDto.getFlightNumber());
        Booking booking = new Booking();
        booking.setFlight(flight);
        booking.setTickets(bookingDto.getTickets());
        booking.setTravelingClass(bookingDto.getTravelingClass());
        booking.getTickets().forEach(ticket -> {
            ticket.setFlight(flight);
        });
        booking.setCustomer(bookingDto.getCustomer());
        return bookingService.createBooking(booking);
    }

    @GetMapping("")
    public List<Booking> findBookings(@PathParam("customerId") String customerId) {
        return this.bookingService.findBookingByCustomerId(customerId);
    }
}
