package com.vdab.backend.restcontrollers;

import com.vdab.backend.domain.*;
import com.vdab.backend.services.AppSettingService;
import com.vdab.backend.services.UserService;
import javassist.NotFoundException;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.annotation.security.RolesAllowed;
import java.util.List;

import static java.lang.Thread.sleep;

@RestController
@RequestMapping("user")
@AllArgsConstructor
public class UserRestController {

    private final UserService userService;
    private final AppSettingService appSettingService;

    @PostMapping("")
    public WebUser createCustomer(@RequestBody Customer customer) {
        return userService.saveUser(customer);
    }

    @GetMapping("{username}")
    public ResponseEntity<WebUser> findCustomer(@PathVariable("username") String username) {
        try {
            return new ResponseEntity<>(userService.findCustomer(username), HttpStatus.ACCEPTED);
        } catch (NotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "User Not Found", e);
        }
    }

    @PutMapping("/settings")
    public void updateSettings(@RequestBody List<AppSettings> appSettings) {
        appSettingService.updateSettings(appSettings);
    }

    @GetMapping("/settings")
    public List<AppSettings> getSettings() {
       return appSettingService.getSettings();
    }
}
