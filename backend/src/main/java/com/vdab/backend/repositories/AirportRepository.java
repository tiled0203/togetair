package com.vdab.backend.repositories;

import com.vdab.backend.domain.Airport;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AirportRepository extends JpaRepository<Airport, Long> {
    void deleteAirportByAirportCode(String airportCode);

    List<Airport> findAirportsByCountryContaining(String country);
}
