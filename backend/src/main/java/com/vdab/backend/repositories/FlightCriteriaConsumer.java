package com.vdab.backend.repositories;

import com.vdab.backend.domain.Flight;
import com.vdab.backend.domain.dto.IsDtoObject;
import com.vdab.backend.domain.dto.FlightCriteria;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import javax.persistence.criteria.*;
import java.lang.reflect.Field;
import java.time.LocalDateTime;
import java.util.function.Consumer;

@Data
@Slf4j
class FlightCriteriaConsumer implements Consumer<Field> {

    private Predicate predicate;
    private CriteriaBuilder criteriaBuilder;
    private Root<Flight> from;
    private FlightCriteria flightCriteria;


    public FlightCriteriaConsumer(Predicate predicate, CriteriaBuilder criteriaBuilder, Root<Flight> from, FlightCriteria flightCriteria) {
        this.predicate = predicate;
        this.criteriaBuilder = criteriaBuilder;
        this.from = from;
        this.flightCriteria = flightCriteria;
    }

    @Override
    public void accept(Field fieldInFlightCriteria) {
        try {
            //fieldInFlightCriteria are the fields in FlightCriteria class and can be for example: flightNumber, destination, departure, departureTime, arrivalTime or travelingClass
            fieldInFlightCriteria.setAccessible(true); //because fields are private , we have to make them accessible, so setAccessible(true) to access them. ONLY DO THIS IF YOU'RE USING JAVA REFLECT API!!
            //fieldInFlightCriteria.getName() ==> gets the name of that specific field and fieldInFlightCriteria.get(flightCriteria) the value of that field in the object flightCriteria
            log.info(fieldInFlightCriteria.getName() + " = " + fieldInFlightCriteria.get(flightCriteria));
            if (fieldInFlightCriteria.get(flightCriteria) != null) {
                if (fieldInFlightCriteria.getType().isEnum()) { //if fieldInFlightCriteria is for Example travelingClass then it returns true
                    //we are only interested in the key of the map availableSeats (see field availableSeats in Flight.class) so with from.joinMap("availableSeats").key() we get the key in table flight_available_seats and column available_seats_key.
                    //WARNING!! : criteriaBuilder creates a JPQL query so we use the field names in our Entity and not the Database column names, so availableSeats and not available_seats_key !
                    //fieldInFlightCriteria.get(flightCriteria) returns the value of the travelingClass field in flightCriteria object, fieldInFlightCriteria is travelingClass see line 41
                    //criteriaBuilder creates JPQL part for the where clause ==> travelingClass = BUSINESS and ......
                    predicate = criteriaBuilder.and(predicate, criteriaBuilder.equal(from.joinMap(fieldInFlightCriteria.getName()).key(), fieldInFlightCriteria.get(flightCriteria)));
                } else if (fieldInFlightCriteria.isAnnotationPresent(IsDtoObject.class)) {
                    //departure and arrival are 2 nested objects in FlightCriteria and are of type AirportDto so we have to fetch the Field names and its values in those nested objects with Reflect Api
                    createNestedObjectPredicate(fieldInFlightCriteria);
                } else if (fieldInFlightCriteria.getType() == LocalDateTime.class) {
                    createDateTimePredicate(fieldInFlightCriteria);
                } else {
                    //any other fields in FlightCriteria
                    predicate = likePredicate(from.get(fieldInFlightCriteria.getName()), (String) fieldInFlightCriteria.get(flightCriteria));
                }
            }
        } catch (IllegalAccessException e) {
            log.error(e.getMessage());
        }
    }

    private void createDateTimePredicate(Field fieldInFlightCriteria) throws IllegalAccessException {
        if (fieldInFlightCriteria.getName().equals("departureTime")) {
            predicate = criteriaBuilder.and(predicate, criteriaBuilder.greaterThanOrEqualTo(from.get(fieldInFlightCriteria.getName()), (LocalDateTime) fieldInFlightCriteria.get(flightCriteria)));
        } else if (fieldInFlightCriteria.getName().equals("arrivalTime")) {
            predicate = criteriaBuilder.and(predicate, criteriaBuilder.between(from.get(fieldInFlightCriteria.getName()), LocalDateTime.now(), (LocalDateTime) fieldInFlightCriteria.get(flightCriteria)));
        }
    }

    private void createNestedObjectPredicate(Field fieldInFlightCriteria) throws IllegalAccessException {
        //iterate over all the AirportDto fields, so fieldInAirportDto could be the Field 'airportCode' for example.
        for (Field fieldInNestedObject : fieldInFlightCriteria.getType().getDeclaredFields()) {
            fieldInNestedObject.setAccessible(true);
            //with fieldInNestedObject.get() we want to fetch the value of that Field, for example for the 'airportCode' field. But 'airportCode' is a member of fieldInFlightCriteria and fieldInFlightCriteria can be for example the arrival or destination Field.
            //then again we can find the values in our flightCriteria object
            //and if fieldInNestedObject is for example the 'airportCode' Field from AirportDto, then we want to check if the value is not null in the flightCriteria object
            String valueOfField = (String) fieldInNestedObject.get(fieldInFlightCriteria.get(flightCriteria));
            if (valueOfField != null) {
                //criteriaBuilder creates JPQL part for the where clause ==> departure like %Brussels% and ...
                //with from.get() we get the values in our database column.
                //from.get(fieldInFlightCriteria.getName()).get(fieldInNestedObject.getName()) === is the f.destination.airportCode part in JPQL, if we would translate this to JPQL then it should look like...
                //SELECT f FROM Flight f WHERE f.destination.airportCode      the like part is made in the likePredicate method
                predicate = likePredicate(from.get(fieldInFlightCriteria.getName()).get(fieldInNestedObject.getName()), valueOfField);
            }
        }
    }

    private Predicate likePredicate(Path<String> path, String value) {
        // the like part of JPQL
        return criteriaBuilder.and(predicate, criteriaBuilder.like(path, "%" + value + "%"));
    }


}
