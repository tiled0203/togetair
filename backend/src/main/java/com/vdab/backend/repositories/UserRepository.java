package com.vdab.backend.repositories;

import com.vdab.backend.domain.WebUser;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

public interface UserRepository extends JpaRepository<WebUser, String> {
    Optional<WebUser> findWebUserByName(String name);
}
