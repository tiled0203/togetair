package com.vdab.backend.repositories;

import com.vdab.backend.domain.AppSettings;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AppSettingsRepository extends JpaRepository<AppSettings, Long> {
    AppSettings findAppSettingsBySettingKey(AppSettings.Settings settings);
}
