package com.vdab.backend.repositories;

import com.vdab.backend.domain.Booking;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface BookingRepository extends JpaRepository<Booking, Long> {
    List<Booking> findBookingsByCustomer_UserId(String customerId);
}
