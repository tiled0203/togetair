package com.vdab.backend.repositories;

import com.vdab.backend.domain.Flight;
import com.vdab.backend.domain.dto.FlightCriteria;

import java.util.List;

public interface SearchFlightRepository {
    public List<Flight> searchFlights(FlightCriteria params);
    List<Flight> searchFlights2(String airline, String departure, String destination);

}
