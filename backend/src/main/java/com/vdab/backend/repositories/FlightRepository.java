package com.vdab.backend.repositories;

import com.vdab.backend.domain.Flight;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface FlightRepository extends PagingAndSortingRepository<Flight, Long>, SearchFlightRepository {
    Flight findFlightByFlightNumber(String flightNumber);
}
