package com.vdab.backend.repositories;

import com.vdab.backend.domain.Flight;
import com.vdab.backend.domain.dto.FlightCriteria;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.Arrays;
import java.util.List;

@Repository
@Slf4j
public class SearchFlightRepositoryImpl implements SearchFlightRepository {
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Flight> searchFlights(FlightCriteria params) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Flight> query = criteriaBuilder.createQuery(Flight.class);
        Root<Flight> from = query.from(Flight.class); //"From Flight"

        Predicate predicate = criteriaBuilder.conjunction();
        FlightCriteriaConsumer searchConsumer = new FlightCriteriaConsumer(predicate, criteriaBuilder, from, params);

        Arrays.stream(params.getClass().getDeclaredFields()).forEach(searchConsumer);
        CriteriaQuery<Flight> criteriaQuery = query.where(searchConsumer.getPredicate());

        return entityManager.createQuery(criteriaQuery).getResultList();
    }

    @Override
    public List<Flight> searchFlights2(String airline, String departure, String destination) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Flight> query = criteriaBuilder.createQuery(Flight.class);
        Root<Flight> from = query.from(Flight.class); //"From Flight"

        Predicate predicate = criteriaBuilder.conjunction();

        if (airline != null) { // where airline.name like %rus% and departure like ...
            predicate = criteriaBuilder.and(predicate, criteriaBuilder.like(from.get("airline").get("name"), "%" + airline + "%"));
        }
        if (departure != null) {
            predicate = criteriaBuilder.and(predicate, criteriaBuilder.like(from.get("departure").get("name"), "%" + departure + "%"));
        }
        if (destination != null) {
            predicate = criteriaBuilder.and(predicate, criteriaBuilder.like(from.get("destination").get("name"), "%" + destination + "%"));
        }

        CriteriaQuery<Flight> criteriaQuery = query.where(predicate);

        return entityManager.createQuery(criteriaQuery).getResultList();

    }
}
