package com.vdab.backend;

import com.vdab.backend.domain.*;
import com.vdab.backend.repositories.AirportRepository;
import com.vdab.backend.repositories.AppSettingsRepository;
import com.vdab.backend.repositories.FlightRepository;
import com.vdab.backend.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDateTime;
import java.util.*;

@SpringBootApplication
@EnableSwagger2
public class BackendApplication implements CommandLineRunner {
    @Autowired
    private AirportRepository airportRepository;

    @Autowired
    private FlightRepository flightRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private AppSettingsRepository appSettingsRepository;

    public static void main(String[] args) {
        SpringApplication.run(BackendApplication.class, args);
    }

    @Override
    public void run(String... args) {
        Map<TravelingClass, Integer> boeingFlight = new EnumMap<>(TravelingClass.class);
        boeingFlight.put(TravelingClass.BUSINESS, 10);
        boeingFlight.put(TravelingClass.ECONOMY, 80);
        boeingFlight.put(TravelingClass.FIRST_CLASS, 10);

        Map<TravelingClass, Integer> smallAirplane = new EnumMap<>(TravelingClass.class);
        smallAirplane.put(TravelingClass.ECONOMY, 200);

        Map<Event, Price> priceMap = new EnumMap<>(Event.class);
        priceMap.put(Event.HOLIDAY, createPrices());
        priceMap.put(Event.WEEKEND, createPrices());
        priceMap.put(Event.NIGHTLY, createPrices());
        Map<Event, Price> priceMap1 = new EnumMap<>(Event.class);
        priceMap1.put(Event.HOLIDAY, createPrices());
        Map<Event, Price> priceMap2 = new EnumMap<>(Event.class);
        priceMap2.put(Event.HOLIDAY, createPrices());
        priceMap2.put(Event.WEEKEND, createPrices());

        Airport ams = new Airport("Amsterdam Airport Schiphol", "AMS", "nl", "Nederland");
        Airport bru = new Airport("Brussels Airport", "BRU", "be", "Belgie");
        Airport fin = new Airport("Helsinki Vantaa Airport", "HEL", "fi", "Finland");
        airportRepository.saveAll(Arrays.asList(ams, bru, fin));
        Airline airline = new Airline("Brussels airline", "test");

        Flight flight = Flight.builder().airline(airline).price(priceMap).flightNumber("1AB").destination(ams).departure(bru).availableSeatsForClass(boeingFlight).departureTime(LocalDateTime.now().plusHours(1L)).arrivalTime(LocalDateTime.now().plusDays(1L)).build();
        Flight flight1 = Flight.builder().price(priceMap1).flightNumber("2AB").destination(bru).departure(ams).availableSeatsForClass(smallAirplane).departureTime(LocalDateTime.now().plusHours(2L)).arrivalTime(LocalDateTime.now().plusDays(2L)).build();
        Flight flight2 = Flight.builder().price(priceMap2).flightNumber("45AB").destination(fin).departure(bru).availableSeatsForClass(boeingFlight).departureTime(LocalDateTime.now().plusHours(1L)).arrivalTime(LocalDateTime.now().plusDays(1L)).build();
        flightRepository.saveAll(Arrays.asList(flight, flight1, flight2));
        AppSettings appSettings = new AppSettings(AppSettings.Settings.PRICE_MARGIN);
        appSettingsRepository.save(appSettings);
        Employee employee = new Employee();
        employee.setAppSettings(appSettings);
        employee.setName("employee");
        userService.saveUser(employee);
        AirlineEmployee airlineEmployee = new AirlineEmployee();
        airlineEmployee.setName("airline");
        userService.saveUser(airlineEmployee);
    }

    private Price createPrices() {
        Map<TravelingClass, Double> basePrises = new EnumMap<>(TravelingClass.class);
        basePrises.put(TravelingClass.BUSINESS, BigDecimal.valueOf(new Random().nextDouble() * 30.0).setScale(2, RoundingMode.CEILING).doubleValue());
        basePrises.put(TravelingClass.ECONOMY, BigDecimal.valueOf(new Random().nextDouble() * 30.0).setScale(2, RoundingMode.CEILING).doubleValue());
        basePrises.put(TravelingClass.FIRST_CLASS, BigDecimal.valueOf(new Random().nextDouble() * 30.0).setScale(2, RoundingMode.CEILING).doubleValue());

        Map<Integer, Integer> volumeDiscounts = new HashMap<>();
        volumeDiscounts.put(5, 10);
        volumeDiscounts.put(2, 5);

        return Price.builder().basePrises(basePrises).volumeDiscounts(volumeDiscounts).build();
    }
}
