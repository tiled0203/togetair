package com.vdab.backend.domain;

public enum Event {
    WEEKEND, HOLIDAY, NIGHTLY
}
