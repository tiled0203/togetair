package com.vdab.backend.domain.dto;

import com.vdab.backend.domain.*;
import lombok.Data;

import java.util.List;

@Data
public class BookingDto {
    private String flightNumber;

    private List<Ticket> tickets;

    private TravelingClass travelingClass;

    private Customer customer;

}
