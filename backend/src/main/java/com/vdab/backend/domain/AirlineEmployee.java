package com.vdab.backend.domain;

import lombok.Getter;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@Getter
@DiscriminatorValue(value = "AIRLINE")
public class AirlineEmployee extends WebUser {

}
