package com.vdab.backend.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Entity
@Getter
@Setter
@DiscriminatorValue(value = "EMPLOYEE")
public class Employee extends WebUser {
    @ManyToOne(cascade = {CascadeType.REFRESH, CascadeType.MERGE})
    private AppSettings appSettings;
}
