package com.vdab.backend.domain;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.util.Map;

@Entity
@Getter
@Builder
@AllArgsConstructor(access = AccessLevel.PROTECTED)
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Price extends AbstractEntity {

    @ElementCollection
    @MapKeyEnumerated(EnumType.STRING)
    @NotEmpty
    private Map<TravelingClass, Double> basePrises;

    /***Key = seats, Value = discount*/
    @ElementCollection
    private Map<Integer, Integer> volumeDiscounts;

    @Setter
    private Double priceMargin;
}
