package com.vdab.backend.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
public class Ticket extends AbstractEntity {

    @ManyToOne(cascade = CascadeType.REFRESH)
    @JoinColumn
    @JsonIgnore
    private Flight flight;

    private String passenger;
}
