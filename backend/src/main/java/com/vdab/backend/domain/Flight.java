package com.vdab.backend.domain;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

@Entity
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class Flight extends AbstractEntity {

    @ManyToOne(cascade = {CascadeType.REFRESH, CascadeType.MERGE, CascadeType.PERSIST})
    private Airline airline;

    @ManyToOne(cascade = {CascadeType.REFRESH})
    private Airport destination;
    @ManyToOne(cascade = {CascadeType.REFRESH})
    private Airport departure;

    private String flightNumber;

    private LocalDateTime departureTime;

    private LocalDateTime arrivalTime;

    @OneToMany(cascade = {CascadeType.REFRESH, CascadeType.MERGE, CascadeType.PERSIST})
    @MapKeyEnumerated(EnumType.STRING)
    private Map<Event, Price> price;

    /**
     * TravelingClass = key , available seats = value
     */
    @ElementCollection
    @MapKeyEnumerated(EnumType.STRING)
    @NonNull
    private Map<TravelingClass, Integer> availableSeatsForClass;

}
