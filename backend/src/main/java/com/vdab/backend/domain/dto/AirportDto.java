package com.vdab.backend.domain.dto;

import lombok.Data;

@Data
public class AirportDto {
    private String name;
    private String airportCode;
}
