package com.vdab.backend.domain;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;



@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class AppSettings extends AbstractEntity {
    public enum Settings {
        PRICE_MARGIN;
    }
    @Setter
    private String settingValue;
    @Enumerated(EnumType.STRING)
    private Settings settingKey;
    public AppSettings(Settings settingKey) {
        this.settingKey = settingKey;
    }
}
