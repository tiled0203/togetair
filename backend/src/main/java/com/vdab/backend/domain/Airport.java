package com.vdab.backend.domain;

import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.Entity;

@Entity
@EqualsAndHashCode(callSuper = true)
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class Airport extends AbstractEntity {
    @NonNull
    private String name;
    private String airportCode;
    private String countryCode;
    private String country;
}
