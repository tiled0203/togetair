package com.vdab.backend.domain;

import lombok.*;
import org.hibernate.validator.constraints.CreditCardNumber;

import javax.persistence.Embeddable;

@Embeddable
@Getter
public class CustomerCreditCard {
    @CreditCardNumber
    private String creditCardNumber;
}
