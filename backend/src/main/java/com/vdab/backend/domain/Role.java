package com.vdab.backend.domain;

public enum Role {
    EMPLOYEE, CUSTOMER, AIRLINE
}
