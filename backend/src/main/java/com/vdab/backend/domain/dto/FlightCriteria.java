package com.vdab.backend.domain.dto;

import com.vdab.backend.domain.TravelingClass;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;

@Data
public class FlightCriteria {
    @IsDtoObject
    private AirportDto destination;
    @IsDtoObject
    private AirportDto departure;

    private String flightNumber;

    @DateTimeFormat(pattern="yyyy/MM/dd HH:mm")
    private LocalDateTime departureTime;

    @DateTimeFormat(pattern="yyyy/MM/dd HH:mm")
    private LocalDateTime arrivalTime;

    private TravelingClass availableSeatsForClass;
}
