package com.vdab.backend.domain;

public enum TravelingClass {
    BUSINESS, FIRST_CLASS, ECONOMY
}
