package com.vdab.backend.domain;

public enum PaymentStatus {
    PAID, PENDING, CANCELLED
}
