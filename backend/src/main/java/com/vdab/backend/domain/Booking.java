package com.vdab.backend.domain;

import lombok.*;

import javax.persistence.*;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Entity
@Getter
@Data
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class Booking extends AbstractEntity {

    @ManyToOne(cascade = {CascadeType.REFRESH})
    private Customer customer;

    @ManyToOne(cascade = {CascadeType.REFRESH, CascadeType.MERGE})
    private Flight flight;

    @OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = "booking_id")
    private List<Ticket> tickets;

    @Enumerated(EnumType.STRING)
    private TravelingClass travelingClass;

    private double price;

    @Enumerated(EnumType.STRING)
    private PaymentStatus paymentStatus;


}
