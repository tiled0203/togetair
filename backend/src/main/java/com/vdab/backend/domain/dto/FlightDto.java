package com.vdab.backend.domain.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.vdab.backend.domain.Airline;
import com.vdab.backend.domain.Airport;
import com.vdab.backend.domain.Price;
import com.vdab.backend.domain.TravelingClass;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.Map;


@Data
public class FlightDto {
    private Airline airline;

    private Airport destination;
    private Airport departure;

    private String flightNumber;

    private LocalDateTime departureTime;

    private LocalDateTime arrivalTime;
    private Map<TravelingClass, Double> ticketPrice;
}
