package com.vdab.backend.services;

import com.vdab.backend.domain.Airline;
import com.vdab.backend.domain.Flight;
import com.vdab.backend.repositories.FlightRepository;
import javassist.NotFoundException;
import org.assertj.core.util.Maps;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class FlightServiceTest {

    @Mock
    private FlightRepository flightRepository;

    @InjectMocks
    private FlightService flightService;

    @BeforeEach
    void setUp() {
        Airline airline = new Airline("BrusselsAirline", "test");

        Flight flight = Flight.builder().availableSeatsForClass(new HashMap<>()).airline(airline).flightNumber("1AB").build();
        Mockito.when(flightRepository.findById(1L)).thenReturn(java.util.Optional.ofNullable(flight));
    }

    @Test
    public void testFindById() throws NotFoundException {
        Flight flight = flightService.findFlight(1L);
        Assertions.assertEquals("1AB", flight.getFlightNumber());
        Assertions.assertEquals("BrusselsAirline", flight.getAirline().getName());
    }

    @Test
    public void verifyFindByIdCalledOnce() throws NotFoundException {
        flightService.findFlight(1L);
        Mockito.verify(flightRepository, Mockito.times(1)).findById(Mockito.any());
        Mockito.verifyNoMoreInteractions(flightRepository);
    }
}
