package com.vdab.backend.services;

import com.vdab.backend.domain.Airport;
import com.vdab.backend.repositories.AirportRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
class AirportServiceTest {

    @Autowired
    private AirportRepository airportRepository;

    @Test
    void name() {
        List<Airport> airports = airportRepository.findAirportsByCountryContaining("Bel");
        Assertions.assertFalse(airports.isEmpty());
    }
}
