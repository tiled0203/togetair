package com.vdab.backend.restcontrollers;

import com.vdab.backend.domain.Flight;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class FlightRestControllerTest {

    private RestTemplate restTemplate = new RestTemplate();

    @Test
    void testSearchFlight() {
        ResponseEntity<Flight[]> forEntity = restTemplate.getForEntity("http://localhost:8080/flights/search", Flight[].class);
        List<Flight> body = Arrays.asList(forEntity.getBody());

        Assertions.assertNotNull(body.get(1).getFlightNumber());
        Assertions.assertEquals("1AB", body.stream().filter(flight -> flight.getFlightNumber().equals("1AB")).findFirst().get().getFlightNumber());

    }
}
